CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * FAQ
 * Maintainers



INTRODUCTION
------------
This is a small module that add a very useful feature for menus blocks created
with the Menu block module. You can hide some selected links when the block is
rendered.

REQUIREMENTS
------------
This module requires the following module:
 * Menu Block (https://www.drupal.org/project/menu_block)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Don't forget to install the Menu block module as well. See :
   https://www.drupal.org/project/menu_block

CONFIGURATION
-------------
* Create a menu block
* Save it and go back to your block
* You will see in the block configuration a new select field called Ignored
links
* Choose the links to hide, (of course the links must be part of the chosen
menu)
* Save, and appreciate the magic!

FAQ
---
Q: There is no "Ignored links" select box in block configuration. Is this
   normal?

A: You must create a menu block, and save it first. Just go back to the
   configuration of this block, and you will see the select box.

MAINTAINERS
-----------
Current maintainers:
 * Nabil Sadki (nabil.sadki) - https://www.drupal.org/u/nabil.sadki
